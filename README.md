# Demo serenity-cucumber automation framework with java

The framework is a serenity-cucumber based BDD framework written in Java and has 1 cucumber feature files with 1 scenario. The scenario automates the below playback funtionality of SBS Video streming website theworldgame.sbs.com.au. The framework uses the page object design pattern and is divided into four layers for ease of development and maintenance.

    1. Feature file
    2. Step definitions file
    3. Steps file
    4. Page file

# Scenario automated

```java
Feature: SBS 'The world game' player test scenario

  Scenario: Play pause interaction with the world game player
    Given the user has navigated to the world game videos page
    When the user plays the video content
    And the user pauses the Ad after 5 seconds if there is a streaming Ad
    And the user plays the Ad again after 2 seconds
    And the user waits for the add to finish
    And the user pauses the video content after 180 seconds for 2 seconds
    Then the user will see that the video content has been paused
```

**Imp note**: 

1. Sometimes the Ad does not starts at begining of the video and the actual media content starts playing without the Ad. In this case, the implentation will ignore the steps about pausing and playing the Ad(since there is no Ad to begin with) and will continue with executing the scenarios written for playing and pausing the actual media content.
2. The requirement to wait for 3 minutes before pausing the video content has been covered in 6th line and can be modified by changing the value in the feature file as desired. The implementation checks for the time elapsed at the bottom of the video streaming window and pauses the window when it reaches 03:00(i.e. 180 secs).
3. The explicit waits in seconds are variables in the 3rd, 4th and 6th line and can be modified in the feature file as per requirement. I have put these so that while running the tests, the end user should be easily able to comprehend that the playback has been paused/played. If you want, you can change these values to '0' in the feature file in which case the playback actions may be too fast to notice.
4. The scenario has one assertion at the last step which checks whether the video has finally been paused. Sometimes and not always after the video is paused, the Ad starts playing. The assertion checks for both the conditions where the Ad may or maynot start after the pause.

## Prerequisite

1. Clone the project into your local machine using git.
2. Make sure Java and Maven has been configured on the system where the test would be executed. (Java version used when creating the framework was 1.8.0_171 and Apache Maven version was 3.3.9).
3. Before executing the tests make sure webdriver-manager is installed and up and listening on port 4444 on local machine. The tests were run on webdriver-manager version 12.1.6. Webdriver-manager can be installed using npm package manager.
4. The test is designed to run on chrome by default and were run on near latest version of chrome 76.0.3809.132. Tests can also be made to run on firefox from the MyRemoteDriver class by simple setting up desired capabilities to firefox in MyRemoteDriver class.
5. The tests have been configured to run parallaly in two browser instances. The number of browser instances can be changed in the pom.xml file under filter <parallel.tests>. However, since there is only 1 feature file, only 1 browser instance would be invoked.

## Executing the test

To run the demo project, clone it and use below maven command from the command line in the project directory.

```java
mvn clean verify serenity:aggregate
```

## Features
1. BDD framework which provides living documentation
2. Cucumber allows variable data to be inserted for the same scenario in the form of data tables for flexible tests using scenario outline
3. The framework can be easily modified to run tests in parallel by modifying fork counts in the pom.xml file.
4. Very detailed report generated with screenshots are provided for each step of the tests and can be modified easily to capture screenshots only on failure.
5. Various Java libraries like pom.xml makes the framework flexible to extend frameworks to feed data from excel sheets.
6. Can be easily made to run on docker containers
7. serenity.properties file allows user to input various important parameters.

## Report
The final report will be generated on the following path
```bash
\\serenity-sbs-demo\target\site\serenity\index.html
```
Below are few screenshots of the report


![image](https://bitbucket.org/saurabh8899/serenity-sbs-demo/raw/640cde15389239cd8cc579bdff0f0c205755f590/src/test/java/demo/auto/example/serenity-report.png)
![image](https://bitbucket.org/saurabh8899/serenity-sbs-demo/raw/640cde15389239cd8cc579bdff0f0c205755f590/src/test/java/demo/auto/example/report-scenario.png)
![image](https://bitbucket.org/saurabh8899/serenity-sbs-demo/raw/640cde15389239cd8cc579bdff0f0c205755f590/src/test/java/demo/auto/example/report-screenshots.png)