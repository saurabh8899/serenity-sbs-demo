package demo.auto.step.definitions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import demo.auto.steps.PlayerInteractionSteps;
import net.thucydides.core.annotations.Steps;




public class PlayerInteractionDefinitions {


    @Steps
    private PlayerInteractionSteps playerInteractionSteps;


    @Given("^the user has navigated to the world game videos page$")
    public void theUserHasNavigatedToTheWorldGameVideosPage() {
        playerInteractionSteps.userHasNavigatedToTheWorldGameVideosPage();

    }

    @When("^the user plays the video content$")
    public void theUserPlaysTheVideoContent() {
        playerInteractionSteps.UserPlaysTheVideoContent();
    }

    @And("^the user pauses the Ad after (.*) seconds if there is a streaming Ad$")
    public void theUserPausesTheAdForSeconds(int seconds) throws InterruptedException {
        playerInteractionSteps.theUserPausesTheAdForSeconds(seconds);
    }

    @And("^the user plays the Ad again after (.*) seconds$")
    public void theUserPlaysTheAdAgain(int seconds) throws InterruptedException {
        playerInteractionSteps.theUserPlaysTheAdAgain(seconds);
    }

    @And("^the user waits for the add to finish$")
    public void theUserWaitsForTheAddToFinish() {
        playerInteractionSteps.theUserWaitsForTheAddToFinish();

    }

    @And("^the user pauses the video content after (.*) seconds for (.*) seconds$")
    public void theUserPausesTheVideoContentAfterSeconds(int seconds, int waitTime) throws InterruptedException {
        playerInteractionSteps.theUserPausesTheVideoContentAfterSeconds(seconds, waitTime);
    }

    @And("^the user will see that the video content has been paused$")
    public void theUserWillSeeThatTheVideoContentHasBeenPaused(){
        playerInteractionSteps.theUserWillSeeThatTheVideoContentHasBeenPaused();
    }



}
