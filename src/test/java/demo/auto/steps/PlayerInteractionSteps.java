package demo.auto.steps;


import demo.auto.pages.WorldGameVideoPage;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;
import static org.junit.Assert.*;
import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

public class PlayerInteractionSteps {


    WorldGameVideoPage worldGameVideoPage;
    Actions actions = new Actions(getDriver());
    JavascriptExecutor jse = (JavascriptExecutor)getDriver();


    @Step
    public void userHasNavigatedToTheWorldGameVideosPage()
    {
        worldGameVideoPage.open();
    }

    @Step
    public void UserPlaysTheVideoContent()
    {
        actions.moveToElement(worldGameVideoPage.getPlayPauseButton()).click().perform();
    }


    @Step
    public void theUserPausesTheAdForSeconds(int seconds) throws InterruptedException {

        Thread.sleep(seconds*1000);
        if(worldGameVideoPage.getVideoContainer().getAttribute("class").contains("is-ad-playing"))
        {
            jse.executeScript("arguments[0].click()", worldGameVideoPage.getPlayPauseButton());
        }
        else
        {
            System.out.println("No Ad was found streaming");
        }
    }

    @Step
    public void theUserPlaysTheAdAgain(int seconds) throws InterruptedException {
        Thread.sleep(seconds*1000);

        if(worldGameVideoPage.getVideoContainer().getAttribute("class").contains("is-ad-paused"))
        {
            jse.executeScript("arguments[0].click()", worldGameVideoPage.getPlayPauseButton());
        }
        else
        {
            System.out.println("No Ad was found to play again in the first place");
        }

    }


    @Step
    public void theUserWaitsForTheAddToFinish() {
        while(worldGameVideoPage.getVideoContainer().getAttribute("class").contains("is-ad-playing"))
        {
            System.out.println("Waiting for Ad to finish");
        }
    }


    @Step
    public void theUserPausesTheVideoContentAfterSeconds(int seconds, int waitTime) throws InterruptedException {

        int minute;
        int secs;
        int totalTimeElapsedInSeconds;

        actions.moveToElement(worldGameVideoPage.getPlayPauseButton());
        String rawTimeElapsed = worldGameVideoPage.getElapsedTimeIdentified().getText();

        minute = Integer.parseInt(rawTimeElapsed.split(":")[0]);
        secs = Integer.parseInt(rawTimeElapsed.split(":")[1]);
        totalTimeElapsedInSeconds = (minute*60)+secs;
        System.out.println(totalTimeElapsedInSeconds);

        while(seconds!=totalTimeElapsedInSeconds){
            rawTimeElapsed = worldGameVideoPage.getElapsedTimeIdentified().getText();
            minute = Integer.parseInt(rawTimeElapsed.split(":")[0]);
            secs = Integer.parseInt(rawTimeElapsed.split(":")[1]);
            totalTimeElapsedInSeconds = minute*60+secs;
        }
        jse.executeScript("arguments[0].click()", worldGameVideoPage.getPlayPauseButton());
        System.out.println("Finishing test now !");
    }


    @Step
    public void theUserWillSeeThatTheVideoContentHasBeenPaused() {
        assertTrue((worldGameVideoPage.getVideoContainer().getAttribute("class").contains("is-media-paused")) || worldGameVideoPage.getVideoContainer().getAttribute("class").contains("is-ad-playing"));
    }


}
