package demo.auto.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebElement;

@DefaultUrl("/video/1372017219530/lineker-s-premier-league-season-so-far-report-part-1?playlist=match-highlights")
public class WorldGameVideoPage extends PageObject {

    @FindBy(xpath="//button[@aria-label='Play']")
    private WebElement playPauseButton;

    @FindBy(xpath="//div[@class='video-experience__container']/div[@data-module='video-player_module']")
    private WebElement videoContainer;

    @FindBy(xpath="//span[@class='video-player__time video-player__time--elapsed']")
    private WebElement elapsedTimeIdentified;


    public WebElement getPlayPauseButton()
    {
        return playPauseButton;
    }

    public WebElement getVideoContainer()
    {
        return videoContainer;
    }

    public WebElement getElapsedTimeIdentified()
    {
        return elapsedTimeIdentified;
    }







}
