Feature: SBS 'The world game' player test scenario

  Scenario: Play pause interaction with the world game player
    Given the user has navigated to the world game videos page
    When the user plays the video content
    And the user pauses the Ad after 5 seconds if there is a streaming Ad
    And the user plays the Ad again after 2 seconds
    And the user waits for the add to finish
    And the user pauses the video content after 180 seconds for 2 seconds
    Then the user will see that the video content has been paused